import subprocess
import re
from collections import Counter

top100 = {'и', 'в', 'не', 'на', 'я', 'быть', 'он', 'с', 'а', 'по',
          'это', 'она', 'этот', 'к', 'но', 'они', 'мы', 'из', 'у',
          'который', 'то', 'за', 'свой', 'что', 'весь', 'год', 'от',
          'так', 'о', 'для', 'ты', 'же', 'все', 'тот', 'мочь', 'вы',
          'человек', 'такой', 'его', 'сказать', 'только', 'или',
          'ещё', 'бы', 'себя', 'один', 'как', 'уже', 'до', 'время',
          'стать', 'при', 'чтобы', 'дело', 'жизнь', 'кто', 'первый',
          'очень', 'два', 'день', 'её', 'новый', 'рука', 'даже', 'во',
          'со', 'раз', 'где', 'там', 'под', 'можно', 'ну', 'какой',
          'после', 'их', 'работа', 'без', 'самый', 'потом', 'надо',
          'хотеть', 'ли', 'слово', 'идти', 'большой', 'должен', 'место',
          'иметь', 'ничто'}


class Analyzer:
    def __init__(self, source_name, encoding='UTF-8'):
        self.source_name = source_name
        cmd = 'mystem.exe -digl ' + self.source_name
        pipe = subprocess.PIPE
        with subprocess.Popen(cmd, shell=True, stdin=pipe, stdout=pipe,
                              stderr=subprocess.STDOUT) as p:
            g = p.stdout
            text = g.read().decode(encoding)[1:-1].split('}{')
        self.analyzed = text

    def find_unique_words(self):
        return len(set(self.analyzed)) / 30000

    @staticmethod
    def find_pattern_frequency(self, pattern):
        c_pattern = re.compile(pattern)
        found = 0
        for word in self.analyzed:
            if re.search(c_pattern, word):
                found += 1
        return found / len(self.analyzed)

    def find_nouns_frequency(self):
        return self.find_pattern_frequency(self, '=S,')

    def find_verbs_frequency(self):
        return self.find_pattern_frequency(self, '=V')

    def find_adjectives_frequency(self):
        return self.find_pattern_frequency(self, '=A=')

    def find_adverbs_frequency(self):
        return self.find_pattern_frequency(self, '=ADV=')

    def find_advpros_frequency(self):
        return self.find_pattern_frequency(self, '=ADVPRO=')

    def find_anums_frequency(self):
        return self.find_pattern_frequency(self, '=ANUM=')

    def find_apros_frequency(self):
        return self.find_pattern_frequency(self, '=APRO=')

    def find_conj_frequency(self):
        return self.find_pattern_frequency(self, '=CONJ=')

    def find_intj_frequency(self):
        return self.find_pattern_frequency(self, '=INTJ=')

    def find_num_frequency(self):
        return self.find_pattern_frequency(self, '=NUM=')

    def find_part_frequency(self):
        return self.find_pattern_frequency(self, '=PART=')

    def find_pr_frequency(self):
        return self.find_pattern_frequency(self, '=PR=')

    def find_spro_frequency(self):
        return self.find_pattern_frequency(self, '=SPRO,')

    def find_notional_speech_part_frequency(self):
        return self.find_pattern_frequency(
            self,
            '=A=|=ADV=|=ADVPRO=|=ANUM=|=APRO=|=INTJ=|=NUM=|=S,|=SPRO,|=V')

    def find_functional_speech_part_frequency(self):
        return self.find_pattern_frequency(self, '=PART=|=PR=|=CONJ=')

    def find_conj_i_frequency(self):
        return self.find_pattern_frequency(self, 'и=CONJ=')

    def find_pr_v_frequency(self):
        return self.find_pattern_frequency(self, 'в=PR=')

    def find_part_ne_frequency(self):
        return self.find_pattern_frequency(self, 'не=PART=')

    def find_spro_ya_frequency(self):
        return self.find_pattern_frequency(self, 'я=SPRO,')

    def find_pr_na_frequency(self):
        return self.find_pattern_frequency(self, 'на=PR=')

    def average_word_length(self):
        pattern_word = re.compile('^(\w+)=')
        quantity = 0
        total_length = 0
        for word in self.analyzed:
            real_word = re.findall(pattern_word, word)
            if len(real_word) >= 1:
                quantity += 1
                total_length += len(real_word[0])
        return total_length / quantity / 100

    @staticmethod
    def find_pattern_in_pattern(self, context, quality):
        c_context = re.compile(context)
        c_quality = re.compile(quality)
        total = 0
        q = 0
        for word in self.analyzed:
            if re.search(c_context, word):
                total += 1
                if re.search(c_quality, word):
                    q += 1
        return q / total

    def find_past_tense(self):
        return self.find_pattern_in_pattern(self, '=V', '=прош|=\(прош')

    def find_not_past_tense(self):
        return self.find_pattern_in_pattern(self, '=V', '=непрош|=\(непрош')

    def find_present_tense(self):
        return self.find_pattern_in_pattern(self, '=V', '=наст|=\(наст')

    def find_infinitive_tense(self):
        return self.find_pattern_in_pattern(self, '=V', '=инф|=\(инф')

    def find_fem_gender(self):
        return self.find_pattern_in_pattern(
            self,
            '=S,|=SPRO,|=V|=A=|=NUM=|=APRO=|=ANUM=',
            'жен')

    def find_male_gender(self):
        return self.find_pattern_in_pattern(
            self,
            '=S,|=SPRO,|=V|=A=|=NUM=|=APRO=|=ANUM=',
            'муж')

    def find_neuter_gender(self):
        return self.find_pattern_in_pattern(
            self,
            '=S,|=SPRO,|=V|=A=|=NUM=|=APRO=|=ANUM=',
            'сред')

    def find_1_most_pop_word(self):
        pattern_word = re.compile('^(\w+)=')
        c = Counter()
        for word in self.analyzed:
            real_word = re.findall(pattern_word, word)
            if len(real_word) >= 1:
                if real_word[0] not in top100:
                    c[real_word[0]] += 1
        return c.most_common(1)[0][1]/len(self.analyzed)
