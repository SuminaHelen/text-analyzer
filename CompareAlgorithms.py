import pandas
from sklearn import model_selection
from sklearn.linear_model import LogisticRegression
from sklearn.tree import DecisionTreeClassifier
from sklearn.neighbors import KNeighborsClassifier
from sklearn.discriminant_analysis import LinearDiscriminantAnalysis
from sklearn.naive_bayes import GaussianNB
from sklearn.svm import SVC

names = ['a', 'adv', 'advpro', 'anum', 'apro', 'conj',
        'intj', 'num', 'part', 'pr', 's', 'spro',
        'verb', 'funct', 'notion', 'i', 'v', 'ne',
        'na', 'ya', 'unique', 'av_len', 'past',
        'not_past', 'pres', 'inf', 'fem', 'male',
        'nue', 'most_pop', 'cls']
dataframe = pandas.read_csv('result/result.cvs', names=names)
array = dataframe.values
X = array[:, 0:30]
Y = array[:, 30]

models = []
models.append(('LR', LogisticRegression()))
models.append(('LDA', LinearDiscriminantAnalysis()))
models.append(('KNN', KNeighborsClassifier()))
models.append(('CART', DecisionTreeClassifier()))
models.append(('NB', GaussianNB()))
models.append(('SVM', SVC()))

results = []
names = []
scoring = 'accuracy'
with open('results/compare_algorithms.txt', 'w') as f:
    for name, model in models:
        cv_results = model_selection.cross_val_score(model, X, Y, scoring=scoring)
        results.append(cv_results)
        names.append(name)
        msg = "%s: %f (%f)" % (name, cv_results.mean(), cv_results.std())
        f.write(msg + '\n')
        print(msg)
