import pandas
from sklearn.feature_selection import RFE
from sklearn.linear_model import LogisticRegression
import os
import pickle


class FeatureSelection:
    def __init__(self):
        if not os.path.exists('result/result.txt'):
            return []
        with open('result/result.txt', 'rb') as f:
            array = pickle.load(f)
        names = ['a', 'adv', 'advpro', 'anum', 'apro', 'conj', 'intj', 'num', 'part', 'pr', 's', 'spro',
                'verb', 'funct', 'notion', 'i', 'v', 'ne', 'na', 'ya', 'unique', 'av_len', 'past',
                'not_past', 'pres', 'inf', 'fem', 'male', 'nue', 'most_pop', 'class']
        dataframe = pandas.read_csv('result/result.cvs', names=names)
        array = dataframe.values
        X = array[:, 0:30]
        Y = array[:, 30]
        model = LogisticRegression()
        rfe = RFE(model, 10)
        fit = rfe.fit(X, Y)
        with open('result/fs.txt', 'w') as fs:
            fs.write("Num Features: {}".format(fit.n_features_) + '\n' +
                     "Selected Features: {}".format(fit.support_) + '\n' +
                     "Feature Ranking: {}".format(fit.ranking_))
            print("Num Features: {}".format(fit.n_features_))
            print("Selected Features: {}".format(fit.support_))
            print("Feature Ranking: {}".format(fit.ranking_))


fs = FeatureSelection()
