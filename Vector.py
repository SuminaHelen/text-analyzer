import pickle
import os.path


class Vector:
    def __init__(self, a, adv, advpro, anum, apro, conj, intj, num, part, pr,
                 s, spro, verb, funct, notion, i, v, ne, na, ya,
                 unique, av_len, past, not_past, pres, inf, fem, male, nue, most_pop, cls):
        self.a = a
        self.adv = adv
        self.advpro = advpro
        self.anum = anum
        self.apro = apro
        self.conj = conj
        self.intj = intj
        self.num = num
        self.part = part
        self.pr = pr
        self.s = s
        self.spro = spro
        self.verb = verb
        self.funct = funct
        self.notion = notion
        self.i = i
        self.v = v
        self.ne = ne
        self.na = na
        self.ya = ya
        self.unique = unique
        self.av_len = av_len
        self.past = past
        self.not_past = not_past
        self.pres = pres
        self.inf = inf
        self.fem = fem
        self.male = male
        self.nue = nue
        self.most_pop = most_pop
        self.cls = cls

    def to_list(self):
        return [self.a, self.adv, self.advpro, self.anum, self.apro, self.conj,
                self.intj, self.num, self.part, self.pr, self.s, self.spro,
                self.verb, self.funct, self.notion, self.i, self.v, self.ne,
                self.na, self.ya, self.unique, self.av_len, self.past,
                self.not_past, self.pres, self.inf, self.fem, self.male,
                self.nue, self.most_pop, self.cls]

    def save(self):
        loaded = self.load()
        with open('result/res30.txt', 'wb') as f:
            loaded.append(self.to_list())
            pickle.dump(loaded, f)

    def load(self):
        if not os.path.exists('result/res30.txt'):
            return []
        with open('result/res30.txt', 'rb') as f:
            return pickle.load(f)
