import pandas
from sklearn.linear_model import LogisticRegression

class Classification:
    def __init__(self):
        names = ['a', 'adv', 'advpro', 'anum', 'apro', 'conj',
                'intj', 'num', 'part', 'pr', 's', 'spro',
                'verb', 'funct', 'notion', 'i', 'v', 'ne',
                'na', 'ya', 'unique', 'av_len', 'past',
                'not_past', 'pres', 'inf', 'fem', 'male',
                'nue', 'most_pop', 'cls']
        new_names = ['s', 'verb', 'funct', 'notion', 'unique',
                     'past','not_past', 'fem', 'male', 'nue', 'cls']
        dataframe = pandas.read_csv('result/res60.cvs', names=names)
        dataframe2 = pandas.read_csv('result/res40.cvs', names=names)

        array = dataframe.values
        array2 = dataframe2.values

        X = array[:, 0:30]
        Y = array[:, 30]
        model = LogisticRegression()
        model.fit(X, Y)

        Xnew = array2[:, 0:30]
        answers = array2[:, 30]
        Ynew = model.predict(Xnew)
        with open('result/classification_with_30_feat.txt', 'w') as fs:
            for i in range(len(Xnew)):
                fs.write("X=%s, Predicted=%s \n" % (answers[i], Ynew[i]))
        for i in range(len(Xnew)):
            print("X=%s, Predicted=%s" % (answers[i], Ynew[i]))

cl = Classification()