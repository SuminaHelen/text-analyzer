import unittest
from TextAnalyzer import Analyzer

example = Analyzer('sources/example.txt')
#print(example.analyzed)
ex2 = Analyzer('sources/verbs.txt')
#print(ex2.analyzed)

class TestingClass(unittest.TestCase):
    def test_example_length(self):
        self.assertEqual(len(example.analyzed), 20)

    def test_example_content_words_frequency(self):
        self.assertEqual(example.find_adjectives_frequency(), 0.05)  # A
        self.assertEqual(example.find_adverbs_frequency(), 0.05)  # ADV
        self.assertEqual(example.find_advpros_frequency(), 0.05)  # ADVPRO
        self.assertEqual(example.find_anums_frequency(), 0.05)  # ANUM
        self.assertEqual(example.find_apros_frequency(), 0.05)  # APRO
        self.assertEqual(example.find_conj_frequency(), 0.05)  # CONJ
        self.assertEqual(example.find_intj_frequency(), 0.05)  # INTJ
        self.assertEqual(example.find_num_frequency(), 0.05)  # NUM
        self.assertEqual(example.find_part_frequency(), 0.1)  # PART
        self.assertEqual(example.find_pr_frequency(), 0.1)  # PR
        self.assertEqual(example.find_nouns_frequency(), 0.2)  # S
        self.assertEqual(example.find_spro_frequency(), 0.1)  # SPRO
        self.assertEqual(example.find_verbs_frequency(), 0.1)  # V

    def test_parts_of_speech_frequency(self):
        self.assertEqual(example.find_functional_speech_part_frequency(), 0.25)
        self.assertEqual(example.find_notional_speech_part_frequency(), 0.75)

    def test_most_popular_words(self):
        self.assertEqual(example.find_conj_i_frequency(), 0.05)
        self.assertEqual(example.find_pr_v_frequency(), 0.05)
        self.assertEqual(example.find_part_ne_frequency(), 0.05)
        self.assertEqual(example.find_pr_na_frequency(), 0.05)
        self.assertEqual(example.find_spro_ya_frequency(), 0.05) #top-5 freq words in rus language

    def test_unique_words(self): #percentage of... not repetition
        self.assertEqual(example.find_unique_words(), 0.0006666666666666666) #all words are unique <3
        self.assertEqual(ex2.find_unique_words(), 0.0008)

    def test_average_word_length(self):
        self.assertEqual(example.average_word_length(), 0.039)
        self.assertEqual(ex2.average_word_length(), 0.0512)

    def test_tenses(self):
        self.assertEqual(ex2.find_past_tense(), 0.3) #прош
        self.assertEqual(ex2.find_not_past_tense(), 0.6) #непрош
        self.assertEqual(ex2.find_present_tense(), 0) #наст
        self.assertEqual(ex2.find_infinitive_tense(), 0.1) #инф

    def test_gender(self):
        self.assertEqual(example.find_fem_gender(), 0.08333333333333333)
        self.assertEqual(ex2.find_fem_gender(), 0.2)
        self.assertEqual(example.find_male_gender(), 0.5)
        self.assertEqual(ex2.find_male_gender(), 0.4)
        self.assertEqual(example.find_neuter_gender(), 0.16666666666666666)
        self.assertEqual(ex2.find_neuter_gender(), 0.05)

    def test_most_pop(self):
        self.assertEqual(example.find_1_most_pop_word(), 0.05)


if __name__ == '__main__':
    unittest.main()
