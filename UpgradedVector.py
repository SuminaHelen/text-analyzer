import pickle
import os


class UpgradedVector:
    def __init__(self, s, verb, funct, notion, unique, past, not_past, fem, male, nue, cls):
        self.s = s
        self.verb = verb
        self.funct = funct
        self.notion = notion
        self.unique = unique
        self.past = past
        self.not_past = not_past
        self.fem = fem
        self.male = male
        self.nue = nue
        self.cls = cls

    def to_list(self):
        return [self.s, self.verb, self.funct, self.notion, self.unique,
                self.past, self.not_past, self.fem, self.male, self.nue, self.cls]

    def save(self):
        loaded = self.load()
        with open('result/res40.txt', 'wb') as f:
            loaded.append(self.to_list())
            pickle.dump(loaded, f)

    def load(self):
        if not os.path.exists('result/res40.txt'):
            return []
        with open('result/res40.txt', 'rb') as f:
            return pickle.load(f)
