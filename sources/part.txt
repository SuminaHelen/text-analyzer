﻿Ах уж темна ночь на небеса всходила,
Уж в городах утих вседневный шум,
Луна в окно Монаха осветила. —
В молитвенник весь устремивший ум,
Панкратий наш Николы пред иконой
Со вздохами земные клал поклоны. —
Пришел Молок (так дьявола зовут),
Панкратия под черной ряской скрылся.
Святой Монах молился уж, молился,
Вздыхал, вздыхал, а дьявол тут как тут.
Бьет час, Молок не хочет отцепиться,
Бьет два, бьет три – нечистый всё сидит.
«Уж будешь мой», – он сам с собой ворчит.
А наш старик уж перестал креститься,
На лавку сел, потер глаза, зевнул,
С молитвою три раза протянулся,
Зевнул опять, и… чуть-чуть не заснул.
Однако ж нет! Панкратий вдруг проснулся,
И снова бес Монаха соблазнять,
Чтоб усыпить, Боброва стал читать.
Монах скучал, Монах тому дивился.
Век не зевал, как богу он молился.
Но – нет уж сил, кресты, псалтирь, слова, —
Всё позабыл; седая голова,
Как яблоко, по груди покатилась,
Со лбу рука в колени опустилась,
Молитвенник упал из рук под стол,
Святой вздремал, всхрапел, как старый вол.