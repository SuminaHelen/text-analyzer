from TextAnalyzer import Analyzer
from UpgradedVector import UpgradedVector
import time
import os


def create_vector(analyzed, class_num):
    return UpgradedVector(analyzed.find_nouns_frequency(),
                  analyzed.find_verbs_frequency(),
                  analyzed.find_functional_speech_part_frequency(),
                  analyzed.find_notional_speech_part_frequency(),
                  analyzed.find_unique_words(),
                  analyzed.find_past_tense(),
                  analyzed.find_not_past_tense(),
                  analyzed.find_fem_gender(),
                  analyzed.find_male_gender(),
                  analyzed.find_neuter_gender(),
                  class_num)

def main():
    time1 = time.time()
    #bulg1 = create_vector(Analyzer('sources/1bulgakov/bel.txt'), 1).save()
    #bulg2 = create_vector(Analyzer('sources/1bulgakov/mim.txt'), 1).save()
    #bulg3 = create_vector(Analyzer('sources/1bulgakov/rok.txt'), 1).save()
    bulg4 = create_vector(Analyzer('sources/1bulgakov/serdce.txt'), 1).save()
    bulg5 = create_vector(Analyzer('sources/1bulgakov/tma.txt'), 1).save()
    #gogol1 = create_vector(Analyzer('sources/2gogol/hutor.txt'), 2).save()
    #gogol2 = create_vector(Analyzer('sources/2gogol/mertvie.txt'), 2).save()
    #gogol3 = create_vector(Analyzer('sources/2gogol/portret.txt'), 2).save()
    gogol4 = create_vector(Analyzer('sources/2gogol/taras.txt'), 2).save()
    gogol5 = create_vector(Analyzer('sources/2gogol/vii.txt'), 2).save()
    #gonch1 = create_vector(Analyzer('sources/3goncharov/dva.txt'), 3).save()
    #gonch2 = create_vector(Analyzer('sources/3goncharov/istoria.txt'), 3).save()
    #gonch3 = create_vector(Analyzer('sources/3goncharov/million.txt'), 3).save()
    gonch4 = create_vector(Analyzer('sources/3goncharov/oblomov.txt'), 3).save()
    gonch5 = create_vector(Analyzer('sources/3goncharov/obryv.txt'), 3).save()
    #dost1 = create_vector(Analyzer('sources/4dost/mertv.txt'), 4).save()
    #dost2 = create_vector(Analyzer('sources/4dost/netochka.txt'), 4).save()
    #dost3 = create_vector(Analyzer('sources/4dost/nochi.txt'), 4).save()
    dost4 = create_vector(Analyzer('sources/4dost/pin.txt'), 4).save()
    dost5 = create_vector(Analyzer('sources/4dost/podp.txt'), 4).save()
    #ch1 = create_vector(Analyzer('sources/5chehov/barinya.txt'), 5).save()
    #ch2 = create_vector(Analyzer('sources/5chehov/futl.txt'), 5).save()
    #ch3 = create_vector(Analyzer('sources/5chehov/monah.txt'), 5).save()
    ch4 = create_vector(Analyzer('sources/5chehov/palata.txt'), 5).save()
    ch5 = create_vector(Analyzer('sources/5chehov/vedma.txt'), 5).save()
    print('............half')
    #kup1 = create_vector(Analyzer('sources/6kuprin/braslet.txt'), 6).save()
    #kup2 = create_vector(Analyzer('sources/6kuprin/gambrinus.txt'), 6).save()
    #kup3 = create_vector(Analyzer('sources/6kuprin/olesya.txt'), 6).save()
    kup4 = create_vector(Analyzer('sources/6kuprin/poedinok.txt'), 6).save()
    kup5 = create_vector(Analyzer('sources/6kuprin/sulamif.txt'), 6).save()
    #ler1 = create_vector(Analyzer('sources/7lermont/ashik.txt'), 7).save()
    #ler2 = create_vector(Analyzer('sources/7lermont/gnv.txt'), 7).save()
    #ler3 = create_vector(Analyzer('sources/7lermont/kalashnikov.txt'), 7).save()
    ler4 = create_vector(Analyzer('sources/7lermont/knagina.txt'), 7).save()
    ler5 = create_vector(Analyzer('sources/7lermont/vadim.txt'), 7).save()
    #push1 = create_vector(Analyzer('sources/8pushkin/barishnya.txt'), 8).save()
    #push2 = create_vector(Analyzer('sources/8pushkin/dubr.txt'), 8).save()
    #push3 = create_vector(Analyzer('sources/8pushkin/kapitanskaya.txt'), 8).save()
    print('...............3/4')
    push4 = create_vector(Analyzer('sources/8pushkin/pikovaya.txt'), 8).save()
    push5 = create_vector(Analyzer('sources/8pushkin/ppipb.txt'), 8).save()
    #solj1 = create_vector(Analyzer('sources/9solj/avgust.txt'), 9).save()
    #solj2 = create_vector(Analyzer('sources/9solj/den.txt'), 9).save()
    #solj3 = create_vector(Analyzer('sources/9solj/matrenin.txt'), 9).save()
    solj4 = create_vector(Analyzer('sources/9solj/rak.txt'), 9).save()
    solj5 = create_vector(Analyzer('sources/9solj/vkruge.txt'), 9).save()
    #turg1 = create_vector(Analyzer('sources/10turg/asya.txt'), 10).save()
    #turg2 = create_vector(Analyzer('sources/10turg/dvoryanskoe.txt'), 10).save()
    #turg3 = create_vector(Analyzer('sources/10turg/nakanune.txt'), 10).save()
    turg4 = create_vector(Analyzer('sources/10turg/oid.txt'), 10).save()
    turg5 = create_vector(Analyzer('sources/10turg/rudin.txt'), 10).save()

    print('...............vectors made')
    time2 = time.time()
    print(time2 - time1)



main()
a = create_vector(Analyzer('sources/10turg/nakanune.txt'), 10)
res = a.load()
for i in res:
    print(i)